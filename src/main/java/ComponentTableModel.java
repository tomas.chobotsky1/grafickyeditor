import javax.swing.table.AbstractTableModel;

public class ComponentTableModel extends AbstractTableModel
{
    private final ComponentList components;
    private final String[] columnNames = { "Name", "Width", "Height", "PosX", "PosY", "Rotation" };
    public ComponentTableModel(ComponentList components)
    {
        this.components = components;
    }
    @Override
    public int getRowCount() { return components.getComponents().size(); }
    @Override
    public int getColumnCount() { return columnNames.length; }
    @Override
    public String getColumnName(int column) { return columnNames[column]; }
    @Override
    public Class<?> getColumnClass(int column)
    {
        return switch (column) {
            case 0 -> String.class;
            case 1, 2, 3, 4 -> Integer.class;
            case 5 -> Double.class;
            default -> Object.class;
        };
    }
    @Override
    public boolean isCellEditable(int row, int column)
    {
        return true;
    }
    @Override
    public Object getValueAt(int row, int column)
    {
        switch (column)
        {
            case 0: return components.getComponents().get(row).getName();
            case 1: return components.getComponents().get(row).getWidth();
            case 2: return components.getComponents().get(row).getHeight();
            case 3: return components.getComponents().get(row).getX();
            case 4: return components.getComponents().get(row).getY();
            case 5: return components.getComponents().get(row).getRotation();
        }
        return "";
    }
    @Override
    public void setValueAt(Object value, int row, int column)
    {
        switch (column)
        {
            case 0: components.getComponents().get(row).setName((String) value); break;
            case 1: components.getComponents().get(row).setWidth((Integer) value); break;
            case 2: components.getComponents().get(row).setHeight((Integer) value); break;
            case 3: components.getComponents().get(row).setX((Integer) value); break;
            case 4: components.getComponents().get(row).setY((Integer) value); break;
            case 5: components.getComponents().get(row).setRotation((Double) value); break;
        }
    }
}
