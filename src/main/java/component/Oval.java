package component;

public class Oval extends BaseComponent
{
    private static int OvalCount = 1;

    public Oval()
    {
        setName("Oval " + OvalCount++);
    }

    public Oval(int x, int y, int width, int height)
    {
        this();
        setX(x);
        setY(y);
        setWidth(width);
        setHeight(height);
    }
}