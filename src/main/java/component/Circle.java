package component;

public class Circle extends BaseComponent
{
    private static int circleCount = 1;

    public Circle()
    {
        setName("Circle " + circleCount++);
    }

    public Circle(int x, int y, int radius)
    {
        this();
        setX(x);
        setY(y);
        setRadius(radius);
    }

    public int getRadius() { return getWidth() / 2; }
    public void setRadius(int radius)
    {
        setWidth(radius * 2);
        setHeight(radius * 2);
    }

    @Override
    public void setWidth(int width)
    {
        super.setWidth(width);
        super.setHeight(width);
    }

    @Override
    public void setHeight(int height)
    {
        super.setWidth(height);
        super.setHeight(height);
    }
}
