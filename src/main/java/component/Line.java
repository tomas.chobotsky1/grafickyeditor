package component;

public class Line extends BaseComponent
{
    private static int lineCount = 1;

    public Line()
    {
        setName("Line " + lineCount++);
    }

    public Line(int x1, int y1, int x2, int y2)
    {
        this();
        setX(x1);
        setY(y1);
        setWidth(x2);
        setHeight(y2);
    }
}
