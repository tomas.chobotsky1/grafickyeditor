package component;

public class Rectangle extends BaseComponent
{
    private static int rectCount = 1;

    public Rectangle()
    {
        setName("Rectangle " + rectCount++);
    }

    public Rectangle(int x, int y, int width, int height)
    {
        this();
        setX(x);
        setY(y);
        setWidth(width);
        setHeight(height);
    }
}
